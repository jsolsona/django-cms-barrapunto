from .xml_barrapunto import parse_rss
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseNotFound
from .models import userdata

index_page = '''
<html>
<head></head>
<body>
    <form method="POST">
        Titulo:<br>
        <input type="text" name="titulo"><br>
        Contenido:<br>
        <input type="text" name="contenido"><br>
        <input type="submit" value="Enviar">
    </form>
{rss_links}
</body>
</html>
'''

def getHTMLRSSCode(url):
    rss_dict = parse_rss("http://barrapunto.com/index.rss")
    rss_html = ""
    for elem in rss_dict:
        rss_html += '<a href="' + rss_dict[elem] + '">' + elem + '</a><br>'
    return rss_html

@csrf_exempt
def index(request):
    if request.method == "GET":

        return HttpResponse(index_page.format(rss_links=getHTMLRSSCode("http://barrapunto.com/index.rss")))
    if request.method == "POST":
        titulo = request.POST['titulo']
        contenido = request.POST['contenido']
        if userdata.objects.filter(nombre=titulo).exists():
            data = userdata.objects.get(nombre=titulo)
            data.contenido = contenido
            data.save()
            return HttpResponse("Data updated")
        else:
            data = userdata(nombre=titulo, contenido=contenido)
            data.save()
            return HttpResponse("Data added")


def getdata(request, nombre):
    if userdata.objects.filter(nombre=nombre).exists():
            return HttpResponse(userdata.objects.get(nombre=nombre).contenido + "<br>" + getHTMLRSSCode("http://barrapunto.com/index.rss"))
    else:
        return HttpResponseNotFound("404 not found")
